package jrtt.imust.com.qmcs;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;



import jrtt.imust.com.qmcs.db.finaldb;

public class MainActivity extends AppCompatActivity {
    private finaldb myFinaldb;
    private ListView lvSj;

    private SQLiteDatabase db;

    private Cursor cursor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvSj = findViewById(R.id.lv_sj);
        myFinaldb = new finaldb(this);
        db = myFinaldb.getReadableDatabase();
        db.execSQL("insert into cdinfo (id,type) values(\"1001\",\"tom\")");
        db.execSQL("insert into cdinfo (id,type) values(\"1002\",\"siri\")");

        cursor = db.rawQuery("select * from cdinfo ",null);

        MyBaseAdapter myBaseAdapter = new MyBaseAdapter();
        lvSj.setAdapter(myBaseAdapter);

    }
    class MyBaseAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return cursor.getCount();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = View.inflate(MainActivity.this,R.layout.item,null);
            TextView tvSj = view1.findViewById(R.id.tv_sj);

            cursor.moveToPosition(i);
            int id = cursor.getInt(0);
            String ty = cursor.getString(1);
            String io = "id = " +id + ";type = "+ty;
            tvSj.setText(io);
            return view1;
        }
    }
}
